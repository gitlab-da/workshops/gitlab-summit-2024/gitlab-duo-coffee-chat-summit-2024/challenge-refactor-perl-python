# Challenge: Refactor Perl into Python

Use GitLab Duo Chat to understand and refactor the provided source code from Perl to Python.

Everything is allowed - except for opening your browser search. Only GitLab Duo interfaces can be used. Chat, Code Suggestions, etc.

Bonus: The code is not documented. Ask Duo Chat to refactor the Perl code with code docs.

## Tips

1. Use [`/explain`](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#explain-code-in-the-ide) to understand what the code does.
1. Use [`/refactor`](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#refactor-code-in-the-ide) with refined prompts.
1. Verify that both scripts are providing the same output on the terminal, using the instructions below, or using the provided CI/CD pipeline.

### Perl

Docker:

```sh
docker run -ti -v `pwd`:/src perl bash

cd /src

perl script.pl 
```

macOS:

```sh
brew install perl

perl script.pl 
```

### Python

```sh
brew install python3

python3 script.py
```

## Solution

[solution/](solution/). 

## Author

@dnsmichi


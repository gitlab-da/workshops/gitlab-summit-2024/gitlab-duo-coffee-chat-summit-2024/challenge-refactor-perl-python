#!/usr/bin/env python

# Refactor put here

import re

with open('file.md') as f:
    lines = f.readlines()

l = len(lines)
e = 0
h = 0

for line in lines:
    if re.match(r'^\s*$', line):
        e += 1
        continue
    
    match = re.match(r'^#+\s*(.+)', line)
    if match:
        print(match.group(1))
        h += 1

print("\nS:")
print(f"L: {l}") 
print(f"E: {e}")
print(f"H: {h}")